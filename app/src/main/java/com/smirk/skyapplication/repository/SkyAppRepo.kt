package com.smirk.skyapplication.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smirk.skyapplication.model.Channels
import com.smirk.skyapplication.model.Rewards

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
object SkyAppRepo {

    private const val sport = "SPORTS"
    private const val kids = "KIDS"
    private const val music = "MUSIC"
    private const val news = "NEWS"
    private const val movies = "MOVIES"

    private var subscribedChannel = ArrayList<String>()
    private val subscribeLiveData = MutableLiveData<String>()


    fun getChannels() : LiveData<List<Channels>> {
        val list = ArrayList<Channels>()

        list.add(Channels(sport, channelSubscribed(sport)))
        list.add(Channels(kids, channelSubscribed(kids)))
        list.add(Channels(music, channelSubscribed(music)))
        list.add(Channels(news, channelSubscribed(news)))
        list.add(Channels(movies, channelSubscribed(movies)))
        return MutableLiveData<List<Channels>>().apply {
            value = list
        }
    }

    private fun channelSubscribed(channel : String) :Boolean{
        return subscribedChannel.contains(channel)
    }

    fun subscribeChannel(channel : String) : LiveData<String>{

        if (!subscribedChannel.contains(channel)) {
            subscribedChannel.add(channel)
            subscribeLiveData.value = "$channel Channel Subscribed"
        }else {
            subscribedChannel.remove(channel)
            subscribeLiveData.value = "$channel Channel unsubscribed"
        }
        return subscribeLiveData
    }

    fun getRewards(accountnumber: String?) : LiveData<Rewards> {

        if(accountnumber == null) {
            return MutableLiveData<Rewards>()
        }

        val r = customerEligibility(accountnumber)
        if (!r.eligibility.equals("CUSTOMER_ELIGIBLE"))
            return MutableLiveData<Rewards>().apply {
                value = r
            }

        val rewardList = ArrayList<String>()
        subscribedChannel.forEach {
            when(it) {
                sport -> {
                    rewardList.add("CHAMPIONS_LEAGUE_FINAL_TICKET")
                }
                music -> {
                    rewardList.add("KARAOKE_PRO_MICROPHONE")
                }
                movies -> {
                    rewardList.add("STAR_WARS_COLLECTION")
                }
            }
        }
        r.rewards = rewardList

        return MutableLiveData<Rewards>().apply {
            value = r
        }
    }


    private fun customerEligibility(accountNumber : String) : Rewards {
        when(accountNumber) {
            "13245" -> {
                return Rewards(accountNumber, description = "Customer is Eligible", eligibility = "CUSTOMER_ELIGIBLE")
            }
            "13246" -> {
                return Rewards(accountNumber, description = "Service technical failure", eligibility = "NO_REWARDS")
            }
            "13247" -> {
                return Rewards(accountNumber, description = "The Supplied account number is invalid", eligibility = "NO_REWARDS")
            }
        }
        return Rewards(accountNumber, description = "Customer is not eligible", eligibility = "NO_REWARDS")
    }

}