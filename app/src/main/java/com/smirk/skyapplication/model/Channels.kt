package com.smirk.skyapplication.model

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
data class Channels(var channelName : String, var isSubscribed : Boolean = false) {
}