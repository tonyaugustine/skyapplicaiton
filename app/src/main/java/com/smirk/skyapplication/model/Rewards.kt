package com.smirk.skyapplication.model

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
data class Rewards(
    var accountNumber: String? = null,
    var rewards: List<String>? = null,
    var description: String? = null,
    var result: String? = null,
    var eligibility: String? = null) {
}