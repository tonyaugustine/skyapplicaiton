package com.smirk.skyapplication.ui.reward

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smirk.skyapplication.app.SkyApp
import com.smirk.skyapplication.repository.SkyAppRepo

class RewardViewModel(private val repo : SkyAppRepo, private val accountNumber : String?) : ViewModel() {


    val rewards = repo.getRewards(accountNumber)

    class Factory(val application: SkyApp, val accountNumber : String?): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RewardViewModel(application.getRepository(), accountNumber) as T
        }

    }
}