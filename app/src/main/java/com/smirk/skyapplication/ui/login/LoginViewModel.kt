package com.smirk.skyapplication.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
class LoginViewModel: ViewModel() {

    private val validation = MutableLiveData<String>()
    private val validationError = MutableLiveData<String>()

    fun validateAccountNumber(accountNumber : String?) {
        if (accountNumber.isNullOrEmpty()) {
            validationError.value = "Please enter valid account number"
        }else {
            validation.value = accountNumber
        }
    }

    fun getValidation() : LiveData<String> {
        return validation
    }

    fun getValidationError() : LiveData<String> {
        return validationError
    }
}