package com.smirk.skyapplication.ui.subscription

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.smirk.skyapplication.R
import com.smirk.skyapplication.databinding.InflateChannelBinding
import com.smirk.skyapplication.model.Channels

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
class AdapterSubscription(private val callback : (Channels) -> Unit) : RecyclerView.Adapter<AdapterSubscription.ViewHolder>() {

    private val channelList = ArrayList<Channels>()

    fun setData(list: List<Channels>) {
        channelList.clear()
        channelList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = DataBindingUtil.inflate<InflateChannelBinding>(LayoutInflater.from(parent.context), R.layout.inflate_channel, parent, false)

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return channelList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.channel = channelList[position]
        holder.binding.executePendingBindings()

        holder.binding.checkBox.setOnClickListener {
            callback(channelList[position])
        }
    }


    class ViewHolder(val binding: InflateChannelBinding) : RecyclerView.ViewHolder(binding.root)
}