package com.smirk.skyapplication.ui.subscription

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.smirk.skyapplication.R
import com.smirk.skyapplication.app.SkyApp
import kotlinx.android.synthetic.main.fragment_subscription.*

class SubscriptionFragment : Fragment() {

    private lateinit var subscriptionViewModel: SubscriptionViewModel
    private lateinit var factory: SubscriptionViewModel.Factory
    private lateinit var adapter : AdapterSubscription

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_subscription, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        factory = SubscriptionViewModel.Factory(requireActivity().application as SkyApp)

        subscriptionViewModel =
            ViewModelProviders.of(this, factory).get(SubscriptionViewModel::class.java)

        adapter = AdapterSubscription {
            subscriptionViewModel.subscribeChannel(it)
        }

        channelList.adapter = adapter

        subscriptionViewModel.getSubscribedData.observe(this, Observer {msg->
            Snackbar.make(parentView, msg, Snackbar.LENGTH_SHORT).show()
        })

        subscriptionViewModel.getChannels().observe(this, Observer {
            it?.let {
                adapter.setData(it)
            }
        })

    }
}