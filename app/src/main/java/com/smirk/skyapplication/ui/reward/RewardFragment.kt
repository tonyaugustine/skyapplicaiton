package com.smirk.skyapplication.ui.reward

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.smirk.skyapplication.MainActivity
import com.smirk.skyapplication.R
import com.smirk.skyapplication.app.SkyApp
import kotlinx.android.synthetic.main.fragment_rewards.*

class RewardFragment : Fragment() {

    private lateinit var rewardViewModel: RewardViewModel
    private lateinit var factory: RewardViewModel.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rewards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        factory = RewardViewModel.Factory(requireActivity().application as SkyApp, (requireActivity() as MainActivity).accountNumber)

        rewardViewModel =
            ViewModelProviders.of(this, factory).get(RewardViewModel::class.java)

        rewardViewModel.rewards.observe(this, Observer {
            if(it != null) {
                val reward = it

                if (reward.eligibility != null) {
                    if(reward.eligibility.equals("CUSTOMER_ELIGIBLE")) {
                        if (!reward.rewards.isNullOrEmpty()) {
                            val builder = StringBuilder()

                            reward.rewards!!.forEach {
                                builder.append(it)
                                    .append("\n")
                            }
                            result.text = builder.toString()
                        }else {
                            result.text = "No Rewards"
                        }
                    }else {
                        result.text = reward.description
                    }
                }else {
                    result.text = "No Rewards"
                }

            }else {
                result.text = "No Rewards"
            }
        })
    }
}