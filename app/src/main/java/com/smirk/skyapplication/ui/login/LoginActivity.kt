package com.smirk.skyapplication.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smirk.skyapplication.MainActivity
import com.smirk.skyapplication.R
import com.smirk.skyapplication.base.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    lateinit var viewModel : LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        go.setOnClickListener {
            viewModel.validateAccountNumber(edAccountNumber.text.toString())
        }

        viewModel.getValidation().observe(this, Observer {
            val intent = Intent(this, MainActivity::class.java).apply {
                putExtra("acn", it)
            }
            startActivity(intent)
            finish()
        })

        viewModel.getValidationError().observe(this, Observer {
            inputlayout.error = it
        })
    }
}
