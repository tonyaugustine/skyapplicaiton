package com.smirk.skyapplication.ui.subscription

import androidx.lifecycle.*
import com.smirk.skyapplication.app.SkyApp
import com.smirk.skyapplication.model.Channels
import com.smirk.skyapplication.repository.SkyAppRepo

class SubscriptionViewModel(private val repo : SkyAppRepo) : ViewModel() {


    fun getChannels() = repo.getChannels()

    private val channelSubscribe = MutableLiveData<String>()

    fun subscribeChannel(channel: Channels) {
        channelSubscribe.value = channel.channelName
    }

    val getSubscribedData : LiveData<String> =
        Transformations.switchMap(channelSubscribe) {data ->
            if (data == null) {
                MutableLiveData<String>()
            }else {
                repo.subscribeChannel(data)
            }

        }

    class Factory(val application: SkyApp): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SubscriptionViewModel(application.getRepository()) as T
        }

    }
}