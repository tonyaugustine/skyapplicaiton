package com.smirk.skyapplication.app

import android.app.Application
import com.smirk.skyapplication.repository.SkyAppRepo

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
class SkyApp : Application() {

    override fun onCreate() {
        super.onCreate()
    }


    fun getRepository() : SkyAppRepo{
        return SkyAppRepo
    }
}