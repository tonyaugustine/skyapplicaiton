package com.smirk.skyapplication.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Tony Augustine on 28,January,2020
 * tonyaugustine47@gmail.com
 */
open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}